module Std
  (
    module Std,
    module Relude,
    module Relude.Extra.Foldable1,
    module Control.Exception.Safe
  )
  where

import Control.Exception.Safe
import qualified Data.List.NonEmpty as N
import qualified Data.Foldable
import Relude.Extra.Foldable1
import Relude hiding (intercalate, some)

(!=) :: (Eq a) => a -> a -> Bool
(!=) = (/=)

infixr 9 .:
(.:) :: (c -> d) -> (a -> b -> c) -> a -> b -> d
(.:) = (.) . (.)

foldr1 :: (Foldable1 f) => (a -> a -> a) -> f a -> a
foldr1 f = Data.Foldable.foldr1 f . toNonEmpty

intercalate :: (Monoid a, Foldable t) => a -> t a -> a
intercalate a = fold . intersperse a . toList

infixl 4 <<*>>
(<<*>>) ::
  (Applicative f, Applicative g) => f (g (a -> b)) -> f (g a) -> f (g b)
(<<*>>) = liftA2 (<*>)

some :: Alternative f => f a -> f (NonEmpty a)
some v = (:|) <$> v <*> many v

bind2 :: Monad m => (a -> b -> m c) -> m a -> m b -> m c
bind2 f a b = uncurry f =<< liftA2 (,) a b

readFileUtf8 :: (MonadIO m) => FilePath -> m Text
readFileUtf8 =
  (=<<) (either (liftIO . throw) pure) .
  fmap decodeUtf8Strict .
  readFileBS

writeFileUtf8 :: (MonadIO m) => FilePath -> Text -> m ()
writeFileUtf8 filePath = writeFileBS filePath . encodeUtf8

sortNonEmptyOn :: Ord b => (a -> b) -> NonEmpty a -> NonEmpty a
sortNonEmptyOn f =
  fmap snd .
  N.sortBy (comparing fst) .
  fmap (\a -> let b = f a in b `seq` (b, a))

-- | fails if the second argument is 'minBound'
fromToUnsafe :: (Enum a) => a -> a -> [a]
fromToUnsafe lower upper = enumFromTo lower (pred upper)

zipPrecisely :: [a] -> [b] -> Maybe [(a, b)]
zipPrecisely [] [] = Just []
zipPrecisely (a:as) (b:bs) = Just $ (a, b) : zip as bs
zipPrecisely _ _ = Nothing

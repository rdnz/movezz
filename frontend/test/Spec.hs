module Main where

import Edit hiding (Court, Range, playerTimelines, timeline)
import qualified Edit as E

import qualified Data.Map.NonEmpty as N
import qualified Data.Map.Strict as M
import Test.Hspec
import Std

main :: IO ()
main = hspec $ do
  describe "Edit" $ do
    describe "processCourtInput, processPlayerTimelinesInput" $ do
      it "discards uncommited" $
        getPlayerTimelines
          (
            -- read sequences of inputs like this from bottom to top
            courtInput Nothing Nothing $
            courtInput (Just "p0") (Just "a0") $
            courtInput (Just "p0") Nothing $
            initialState
          )
        `shouldBe`
        []
      it "commits on edit" $
        getPlayerTimelines
          (
            courtInput Nothing Nothing $
            playerTimelinesInput "p0" 0 (Right 7) $
            courtInput (Just "p0") (Just "a0") $
            courtInput (Just "p0") Nothing $
            initialState
          )
        `shouldBe`
          [
            (
              "p0",
              [Range "a0" 5 7]
            )
          ]

-- $> main

data Court =
  Court
    -- | the marked player on the court widget
    (Maybe Text)
    -- | the marked arrow on the court widget
    (Maybe Text)
  deriving (Eq, Show)

data Range =
  Range
    -- | the arrow
    Text
    -- | the start time
    Integer
    -- | the end time
    Integer
  deriving (Eq, Show)

getCourt ::
  (E.Court, PlayerTimelines, Maybe Player) -> Court
getCourt (E.Court player arrow, _, _) =
  Court (coerce player) (coerce arrow)

getPlayerTimelines ::
  (E.Court, PlayerTimelines, Maybe Player) -> [(Text, [Range])]
getPlayerTimelines (_, PlayerTimelines playerTimelines, _) =
  coerce $
  M.toList $
  fmap toList $
  (fmap . fmap)
    (\(E.Range a s e) -> Range (coerce a) (coerce s) (coerce e)) $
  fmap unTimeline $
  playerTimelines

getUncommited ::
  (E.Court, PlayerTimelines, Maybe Player) -> Maybe Text
getUncommited (_, _, uncommited) = coerce uncommited

initialState :: (E.Court, PlayerTimelines, Maybe Player)
initialState = (E.Court Nothing Nothing, PlayerTimelines M.empty, Nothing)

courtInput ::
  -- | player
  Maybe Text ->
  -- | arrow
  Maybe Text ->
  -- | old state
  (E.Court, PlayerTimelines, Maybe Player) ->
  -- | new state
  (E.Court, PlayerTimelines, Maybe Player)
courtInput playerId arrowId (_courtOld, playerTimelinesOld, uncommitedOld)
  = (courtNew, playerTimelinesNew, uncommitedNew)
  where
    (playerTimelinesNew, uncommitedNew) =
      processCourtInput courtNew playerTimelinesOld uncommitedOld
    courtNew = E.Court (Player <$> playerId) (Arrow <$> arrowId)

playerTimelinesInput ::
  -- | player
  Text ->
  -- | index
  Integer ->
  -- | start time or end time
  Either Integer Integer ->
  -- | old state
  (E.Court, PlayerTimelines, Maybe Player) ->
  -- | new state
  (E.Court, PlayerTimelines, Maybe Player)
playerTimelinesInput
  playerId
  index
  rangeInputRaw
  (_courtOld, playerTimelinesOld0, uncommitedOld)
  = (courtNew, playerTimelinesNew, uncommitedNew)
  where
    (courtNew, uncommitedNew) =
      processPlayerTimelinesInput
        (playerTimelinesNew, (player, index, rangeInput))
    playerTimelinesNew =
      fromMaybe
        playerTimelinesOld1
        (processPlayerTimelinesInputToDiscardUncommited
          (playerTimelinesOld1, (player, index, rangeInput))
          uncommitedOld
        )
      where
        playerTimelinesOld1 =
          -- yes, this needs to stay in synchronization with the player
          -- timeline widget's real behavior. this risk is justified
          -- because it is the nature of a mock to exhibit this problem.
          PlayerTimelines
            (M.alter
              (\case
                Nothing -> error "input for a non-existing player"
                Just timeline ->
                  Just $
                  Timeline $
                  N.alter'
                    (\case
                      Nothing -> error "input for a non-existing index"
                      Just (E.Range arrow startOld endOld) ->
                        case rangeInput of
                          Left startNew -> E.Range arrow startNew endOld
                          Right endNew -> E.Range arrow startOld endNew
                    )
                    index
                    (coerce timeline)
              )
              player
              (coerce playerTimelinesOld0)
            )
    player = Player playerId
    rangeInput :: Either Time Time
    rangeInput = coerce rangeInputRaw

{-# language
  RecursiveDo,
  ApplicativeDo,
  TemplateHaskell
#-}

module Utility where

import Control.Lens (over)
import Control.Monad.Fix (MonadFix)
import Data.Functor.Misc (Const2 (Const2))
import Data.Map.NonEmpty (NEMap)
import qualified Data.Map.NonEmpty as N
import qualified Data.Map.NonEmpty.Internal as NUnsafe
import qualified Data.Map.Strict as M
import Data.Semialign (align)
import Data.Semigroup (Max (Max))
import Data.These (These (That, These, This))
import Reflex.Dom.Core hiding (setValue, rangeInput)
import Std hiding (mapMaybe)

-- | like 'domEvent' but stop the event from bubbling up the DOM
domEventWithoutPropagation ::
  forall t m eventName.
  (DomBuilder t m) =>
  EventName eventName ->
  RawElement (DomBuilderSpace m) ->
  m (Event t (EventResultType eventName))
domEventWithoutPropagation eventName rawElement =
  domEvent eventName <$>
    wrapRawElement
      rawElement
      (
        over
          (rawElementConfig_eventSpec @_ @_ @(DomBuilderSpace m))
          (
            addEventSpecFlags
              (Proxy @(DomBuilderSpace m))
              eventName
              (const stopPropagation)
          ) $
        def
      )

-- | stronger version of 'Reflex.Collection.listWithKey' because
-- @(v, Event t v)@ can be converted to @Dynamic t v@ via
-- 'Reflex.Dynamic.holdDyn' but not the other way around
listWithKeyWithEvent ::
  forall t k v m a.
  (Ord k, Adjustable t m, MonadFix m, MonadHold t m) =>
  (k -> v -> Event t v -> m a) ->
  Map k v ->
  Event t (Map k v) ->
  m (Dynamic t (Map k a)) -- to-do. (a, Event t a)
listWithKeyWithEvent mkChild initialValues setValues =
  do
    rec
      sentVals :: Dynamic t (Map k v) <-
        foldDyn applyMap initialValues changeVals
      let
        changeVals :: Event t (Map k (Maybe v))
        changeVals =
          attachWith diffOnlyKeyChanges (current sentVals) setValues
    listHoldWithKey initialValues changeVals $ \k v ->
      mkChild k v (select childValChangedSelector (Const2 k))
  where
    childValChangedSelector :: EventSelector t (Const2 k v)
    childValChangedSelector = fanMap setValues
    diffOnlyKeyChanges :: (Ord b) => Map b c -> Map b d -> Map b (Maybe d)
    diffOnlyKeyChanges olds news =
      flip M.mapMaybe (align olds news) $ \case
        This _    -> Just Nothing
        These _ _ -> Nothing
        That new  -> Just $ Just new

-- | 'Reflex.Class.attachWith' with an '(<*>)' like interface
attachAp :: (Reflex t) => Event t (a -> b) -> Behavior t a -> Event t b
attachAp e b = attachWith (flip ($)) b e
infixl 4 `attachAp`

maximum1NonEmpty :: forall a k. (Ord a) => NEMap k a -> a
maximum1NonEmpty = coerce (N.foldl1' (<>) :: NEMap k (Max a) -> (Max a))

-- Data.Map.NonEmpty.deleteMax is faulty until version 0.3.4.3
deleteMax :: NEMap k a -> Map k a
deleteMax (NUnsafe.NEMap k v m) = case M.maxView m of
    Nothing      -> M.empty
    Just (_, m') -> NUnsafe.insertMinMap k v m'

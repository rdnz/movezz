{-# language
  RecursiveDo,
  ApplicativeDo,
  TemplateHaskell
#-}

module Edit where

import Utility
  (
    attachAp,
    deleteMax,
    maximum1NonEmpty,
    listWithKeyWithEvent,
    domEventWithoutPropagation
  )

import Text.Pretty.Simple (pShowNoColor)
import Control.Monad.Fix (MonadFix)
import Data.Map.NonEmpty (NEMap)
import qualified Data.Map.NonEmpty as N
import qualified Data.Map.Strict as M
import qualified Data.Semialign as A
import GHCJS.DOM.DOMTokenList (add, remove)
import GHCJS.DOM.Document (getElementsByClassName)
import GHCJS.DOM.Element (getClassList, getId)
import GHCJS.DOM.HTMLCollection (getLength, item)
import GHCJS.DOM.HTMLObjectElement (getContentDocument)
import GHCJS.DOM.NonElementParentNode (getElementById)
import GHCJS.DOM.ParentNode (getChildren)
import qualified GHCJS.DOM.Types as J
import Obelisk.Generated.Static (static)
import Reflex.Dom.Core hiding (setValue, rangeInput)
import Std hiding (mapMaybe)

newtype Arrow =
  Arrow Text
  deriving (Eq, Ord) via Text
  deriving (Show)
newtype Time =
  Time Integer
  deriving (Eq, Ord, Enum, Num, Real, Integral) via Integer
  deriving (Show)
newtype Player =
  Player Text
  deriving (Eq, Ord) via Text
  deriving (Show)

newtype PlayerTimelines =
  PlayerTimelines (Map Player Timeline)
  deriving (Eq) via Map Player Timeline
  deriving (Show)

newtype Timeline =
  Timeline (NEMap Integer Range)
  deriving (Eq) via NEMap Integer Range
  deriving (Show)

unTimeline :: Timeline -> NEMap Integer Range
unTimeline (Timeline l) = l

data Range =
  Range Arrow Time Time
  deriving (Eq, Show)

rangeArrow :: Range -> Arrow
rangeArrow (Range a _s _e) = a
rangeStart :: Range -> Time
rangeStart (Range _a s _e) = s
rangeEnd :: Range -> Time
rangeEnd (Range _a _s e) = e

data Court =
  Court (Maybe Player) (Maybe Arrow)
  deriving (Eq, Show)

timeLength :: Time
timeLength = 15

view ::
  (DomBuilder t m, MonadHold t m, Prerender t m, MonadFix m) =>
  m (Dynamic t PlayerTimelines)
view =
  elAttr "div" ("id" =: "main" <> "style" =: mainStyle) $ mdo
    -- controller
    (initialCourt, setCourt, initialPlayerTimelines, setPlayerTimelines, playerTimelinesValue) <-
      controller
        courtInput
        playerTimelinesInput
    -- court widget
    courtInput <- court initialCourt setCourt
    el "br" blank
    playerTimelinesInput <- el "div" $
      -- player timelines widget
      playerTimelines
        initialPlayerTimelines
        setPlayerTimelines <*
      -- log player timelines widget value for debugging
      el "br" blank <*
      el "div" (text "log for debugging") <*
      elAttr "pre" ("style" =: "font-size:x-small")
        (textNode
          (
            textNodeConfig_setContents
              .~ (toStrict . pShowNoColor <$> updated playerTimelinesValue) $
            textNodeConfig_initialContents
              .~ (toStrict $ pShowNoColor $ initialPlayerTimelines) $
            def
          )
        )
    pure playerTimelinesValue
  where
    mainStyle =
      "font: 16px / 1.2 sans-serif; \
      \width: 500px; \
      \margin: 0 auto;"

-- | update court widget value and player timelines widget value
-- based on court widget input and player timelines widget input
controller ::
  forall t m.
  (Reflex t, MonadHold t m, MonadFix m) =>
  -- | court widget input
  Event t (Court, Maybe (Either Player Arrow)) ->
  -- | player timelines widget input
  Event t (PlayerTimelines, (Player, Integer, Either Time Time)) ->
  m
    (
      Court, -- initial court widget value
      Event t Court, -- set court widget value
      PlayerTimelines, -- initial player timelines widget value
      Event t PlayerTimelines, -- set player timelines widget value
      Dynamic t PlayerTimelines -- player timelines widget value
    )
controller courtInput playerTimelinesInput =
  mdo
    -- courtValue <-
    --   holdDyn
    --     initialCourt
    --     (leftmost [setCourtOnPlayerTimelinesInput, fst <$> courtInput])
    playerTimelinesValue <-
      holdDyn
        initialPlayerTimelines
        (leftmost
          [
            setPlayerTimelinesOnCourtInput,
            setPlayerTimelinesOnPlayerTimelinesInput,
            fst <$> playerTimelinesInput
          ]
        )
    uncommited :: Behavior t (Maybe Player) <-
      hold
        Nothing
        (leftmost
          [setUncommitedOnCourtInput, setUncommitedOnPlayerTimelinesInput]
        )
    let
      setOnCourtInput =
        processCourtInput
          <$> (fst <$> courtInput)
          `attachAp` current playerTimelinesValue
          `attachAp` uncommited
      setPlayerTimelinesOnCourtInput = fst <$> setOnCourtInput
      setUncommitedOnCourtInput = snd <$> setOnCourtInput
      setOnPlayerTimelinesInput =
        processPlayerTimelinesInput
          <$>
            leftmost
              [
                A.zip
                  setPlayerTimelinesOnPlayerTimelinesInput
                  (snd <$> playerTimelinesInput),
                playerTimelinesInput
              ]
      setCourtOnPlayerTimelinesInput = fst <$> setOnPlayerTimelinesInput
      setUncommitedOnPlayerTimelinesInput =
        snd <$> setOnPlayerTimelinesInput
      setPlayerTimelinesOnPlayerTimelinesInput =
        mapMaybe id
          (processPlayerTimelinesInputToDiscardUncommited
            <$> playerTimelinesInput
            `attachAp` uncommited
          )
    pure
      (
        initialCourt,
        setCourtOnPlayerTimelinesInput,
        initialPlayerTimelines,
        leftmost
          [
            setPlayerTimelinesOnCourtInput,
            setPlayerTimelinesOnPlayerTimelinesInput
          ],
        playerTimelinesValue
      )
  where
    initialCourt = Court Nothing Nothing
    initialPlayerTimelines = PlayerTimelines M.empty

-- | process court widget input
processCourtInput ::
  -- | new court widget value
  Court ->
  -- | player timelines widget value
  PlayerTimelines ->
  -- | uncommited player timelines addition
  Maybe Player ->
  (PlayerTimelines, Maybe Player)
processCourtInput
  courtValue playerTimelinesValue uncommitedMaybe =
  case courtValue of
    Court (Just player) (Just arrow) -> addRange player arrow
    _ -> \t -> (t, Nothing)
  $
  case uncommitedMaybe of
    Nothing -> id
    Just uncommited ->
      coerce $
        M.alter
          (\case
            Just (Timeline timelineOld) ->
              -- Data.Map.NonEmpty.deleteMax is faulty until version 0.3.4.3
              fmap Timeline $ N.nonEmptyMap $ deleteMax $ timelineOld
            Nothing -> error "impossible. if there is an uncommited range for that player, there is a range for that player." -- to-do. move up
          )
          uncommited
  $
  playerTimelinesValue
  where
    addRange ::
      Player ->
      Arrow ->
      PlayerTimelines ->
      (PlayerTimelines, Maybe Player)
    addRange player arrow (PlayerTimelines playerTimelinesOld) =
      (
        PlayerTimelines
          (M.alter
            (
              Just .
              Timeline .
              \case
                Nothing ->
                  N.singleton 0 (Range arrow start end)
                  where
                    start = timeLength `div` 3
                    end = max (start + 1) (start * 2)
                Just (Timeline timelineOld) ->
                  N.insert
                    (succ $ fst $ N.findMax $ timelineOld)
                    (Range arrow (maxOld + start) (maxOld + end))
                    timelineOld
                  where
                    start = (timeLength - maxOld) `div` 3
                    end = max (start + 1) (start * 2)
                    maxOld = maximum1NonEmpty $ fmap rangeEnd $ timelineOld
            )
            player
            playerTimelinesOld
          )
        ,
        Just player
      )

-- | process player timelines widget input
processPlayerTimelinesInput ::
  -- | player timelines widget input
  (PlayerTimelines, (Player, Integer, Either Time Time)) ->
  (Court, Maybe Player)
processPlayerTimelinesInput
  (PlayerTimelines playerTimelinesValue, (player, index, _))
  = (
    Court (Just player) $
      Just $
      rangeArrow $
      fromMaybe (error "impossible.") $ -- to-do. move up
      (=<<) (N.lookup index) $ -- Maybe Monad
      coerce $
      M.lookup player playerTimelinesValue
    ,
    Nothing
  )

processPlayerTimelinesInputToDiscardUncommited ::
  (PlayerTimelines, (Player, Integer, Either Time Time)) ->
  Maybe Player ->
  Maybe PlayerTimelines
processPlayerTimelinesInputToDiscardUncommited
  (PlayerTimelines playerTimelinesValueOld, (player, index, _))
  (Just uncommited)
  | Just (Timeline t) <- M.lookup uncommited playerTimelinesValueOld =
    if index == fst (N.findMax t) && player == uncommited
    then Nothing
    else
      Just $
      PlayerTimelines $
      M.alter
        (\case
          -- Data.Map.NonEmpty.deleteMax is faulty until version 0.3.4.3
          Just u -> fmap Timeline $ N.nonEmptyMap $ deleteMax $ coerce $ u
          Nothing -> error "impossible. Just (Timeline t) <- M.lookup uncommited playerTimelinesValueOld"
        )
        uncommited
        playerTimelinesValueOld
  | otherwise =
    error "impossible. if there is an uncommited range for that player, there is a range." -- to-do. move up
processPlayerTimelinesInputToDiscardUncommited _ Nothing = Nothing

-- | player timelines widget.
playerTimelines ::
  (DomBuilder t m, MonadHold t m, MonadFix m) =>
  -- | initial value
  PlayerTimelines ->
  -- | set value
  Event t PlayerTimelines ->
  m (Event t (PlayerTimelines, (Player, Integer, Either Time Time)))
playerTimelines initialValue setValue =
  (\listResult ->
    patchValue
      <$> (
        fmap fromMapUnsafe $
        switchDyn $
        fmap mergeMap $
        (fmap . fmap) fst $
        listResult
      )
      `attachAp` (
        fmap PlayerTimelines $
        current $
        joinDynThroughMap $
        (fmap . fmap) snd $
        listResult
      )
  )
    <$>
      listWithKeyWithEvent
        (\(Player p) -> el "div" .: (el "div" (text p) *>) .: timeline)
        (coerce initialValue)
        (coerce <$> setValue)
  where
    -- to-do. there must be a way to avoid this
    patchValue ::
      (Player, Integer, Either Time Time) ->
      PlayerTimelines ->
      (PlayerTimelines, (Player, Integer, Either Time Time))
    patchValue
      input@(player, index, rangeInput)
      (PlayerTimelines playerTimelinesOld)
      = (
        PlayerTimelines
          (M.alter
            (\case
              Nothing -> error "impossible"
              Just u ->
                Just $
                Timeline $
                N.alter'
                  (\case
                    Nothing -> error "impossible"
                    Just (Range arrow startOld endOld) ->
                      case rangeInput of
                        Left startNew -> Range arrow startNew endOld
                        Right endNew -> Range arrow startOld endNew
                  )
                  index
                  (coerce u)
            )
            player
            playerTimelinesOld
          )
        ,
        input
      )
    fromMapUnsafe m =
      (\case {[(p, [(i, r)])] -> (p, i, r); _ -> error "impossible. listWithKeyWithEvent does not return empty maps and there can only be at most one user input at a time."}) $
      M.toList $
      fmap M.toList $
      m

-- to-do. abstract multiRangeSlider out
-- | timeline widget
timeline ::
  (DomBuilder t m, MonadHold t m, MonadFix m) =>
  Timeline ->
  Event t Timeline ->
  m (Event t (Map Integer (Either Time Time)), Dynamic t Timeline)
timeline initialValue setValue =
  elAttr "div" ("style" =: timelineSytle) $
    elAttr "div" ("style" =: timelineBackgroundStyle) blank *>
    (
      fmap
        (\listResult ->
          (
            switchDyn $
              fmap mergeMap $
              (fmap . fmap) fst $
              listResult
            ,
            fmap Timeline $
              fmap (fromMaybe $ error "impossible.") $ -- to-do
              fmap N.nonEmptyMap $
              joinDynThroughMap $
              (fmap . fmap) snd $
              listResult
          )
        ) $
      listWithKeyWithEvent
        (const rangeSlider)
        (N.toMap $ unTimeline $ initialValue)
        (
          fmap N.toMap $
          fmap unTimeline $
          setValue
        )
    )
  where
    timelineBackgroundStyle =
      "grid-column: 1; \
      \grid-row: 1; \
      \align-self: center; \
      \height: 8px; \
      \background-color: lightgray;"
    timelineSytle =
      "display: grid; \
      \grid-template-columns: auto; \
      \grid-template-rows: auto;"

-- | range slider widget
rangeSlider ::
  (DomBuilder t m, MonadHold t m) =>
  Range ->
  Event t Range ->
  m (Event t (Either Time Time), Dynamic t Range)
rangeSlider initialValue@(Range initialArrow _ _) setValue =
  (\arrow (input0, value0) (input1, value1) ->
    (
      leftmost [Left <$> input0, Right <$> input1]
      ,
      Range <$> arrow <*> value0 <*> value1
    )
  )
    <$> holdDyn initialArrow (rangeArrow <$> setValue)
    <*> slider StartSlider (rangeStart initialValue) (rangeStart <$> setValue)
    <*> slider EndSlider (rangeEnd initialValue) (rangeEnd <$> setValue)

data SliderType = StartSlider | EndSlider

-- | slider widget
slider ::
  (DomBuilder t m, MonadHold t m) =>
  SliderType -> Time -> Event t Time -> m (Event t Time, Dynamic t Time)
slider sliderType initialTime@(Time initialValue) setValue =
  (=<<)
    (\input ->
      (input,) <$> holdDyn initialTime (leftmost [setValue, input])
    ) $
  (fmap . fmap) toTimeUnsafe $
  fmap _inputElement_input $
  inputElement
    (
      initialAttributes
        .~
          (
            "type" =: "range" <>
            "min" =: "0" <>
            "max" =: (let Time t = timeLength in show t) <>
            "step" =: "1" <>
            "style" =: sliderStyle <>
            "class"
              =:
                case sliderType of
                  StartSlider -> "start_slider"
                  EndSlider -> "end_slider"
          ) $
      inputElementConfig_setValue
        .~ (fmap show $ fmap (\(Time t) -> t) $ setValue) $
      inputElementConfig_initialValue .~ show initialValue $
      def
    )
  where
    toTimeUnsafe :: Text -> Time
    toTimeUnsafe =
      Time .
      fromMaybe
        (error "impossible. a slider with step of 1 returns integers only.") .
      readMaybe .
      toString
    sliderStyle :: Text
    sliderStyle =
      "grid-column: 1; \
      \grid-row: 1; \
      \appearance: none; \
      \visibility: hidden;"

-- | court widget.
court ::
  (DomBuilder t m, MonadHold t m, Prerender t m, MonadFix m) =>
  -- | initial value
  Court ->
  -- | set value
  Event t Court ->
  m (Event t (Court, Maybe (Either Player Arrow)))
court initialValue setValue =
  mdo
    value <-
      foldDyn
        processInputCourt
        initialValue
        (leftmost [Left <$> setValue, Right <$> input])
    input <-
      fmap switchDyn $
      prerender (never <$ dom) $
      (do
        c <- dom
        fmap switchDyn $
          widgetHold (pure never) $
          (<$)
            (do
              svg <-
                getContentDocument $
                J.uncheckedCastTo J.HTMLObjectElement $
                _element_raw $
                c
              performEvent_ $
                mark
                  svg
                  <$> updated value
                  `attachAp` current value
              players <-
                getElementsByClassName svg ("player" :: Text)
              playerClick <-
                (fmap . fmap) Player $ clickEventWithId players
              arrows <-
                getElementsByClassName svg ("arrow" :: Text)
              arrowClick <-
                (fmap . fmap) Arrow $ clickEventWithId arrows
              svgClick <-
                fmap (domEvent Click) $
                (=<<) (flip wrapRawElement def) $
                fmap (fromMaybe $ error "court svg is empty") $
                (=<<) (flip item 0) $
                getChildren $
                svg
              pure $
                leftmost
                  [
                    (Just . Right) <$> arrowClick,
                    (Just . Left) <$> playerClick,
                    Nothing <$ svgClick
                  ]
            ) $
          domEvent Load $
          c
      )
    pure
      (
        (\i v -> (processInputCourt (Right i) v, i))
          <$> input
          `attachAp` current value
      )
  where
    dom ::
      (DomBuilder t m) =>
      m (Element EventResult (DomBuilderSpace m) t)
    dom =
      fst <$>
        elAttr'
          "object"
          (
            "id" =: "court" <>
            "type" =: "image/svg+xml" <>
            "data" =: $(static "court.svg") <>
            "style" =: courtStyle
          )
          blank
    clickEventWithId ::
      (DomBuilder t m, J.MonadJSM m, DomBuilderSpace m ~ GhcjsDomSpace) =>
      J.HTMLCollection -> m (Event t Text)
    clickEventWithId elements =
      do
        elementsCount <- getLength elements
        fmap leftmost $
          fmap
            (fromMaybe $ error "impossible. indexes are less than length.") $
          getCompose $
          traverse
            (\index -> Compose $ -- Maybe
              item elements index
                >>= \case
                  Nothing -> pure Nothing
                  Just rawElement ->
                    do
                      i :: Text <- getId rawElement
                      fmap Just $
                        (fmap . fmap) (const i) $
                        domEventWithoutPropagation Click rawElement
            )
            (case elementsCount of
              0 -> []
              _ -> fromToUnsafe 0 elementsCount
            )
    mark ::
      forall m. (J.MonadJSM m) => J.Document -> Court -> Court -> m ()
    mark svg (Court playerNew arrowNew) (Court playerOld arrowOld) =
      markById (coerce playerOld) remove *>
      markById (coerce arrowOld) remove *>
      markById (coerce playerNew) add *>
      markById (coerce arrowNew) add
      where
        markById :: Maybe Text -> (J.DOMTokenList -> [Text] -> m ()) -> m ()
        markById Nothing _ = pure ()
        markById (Just i) f =
          getElementById svg i
            >>= \case
              Nothing -> pure ()
              Just e -> flip f ["mark"] =<< getClassList e
    courtStyle :: Text
    courtStyle = "width: 100%;"

processInputCourt ::
  Either Court (Maybe (Either Player Arrow)) -> Court -> Court
processInputCourt (Left c) _ = c
processInputCourt (Right Nothing) _ = Court Nothing Nothing
processInputCourt (Right (Just (Left player))) (Court _ arrow) =
  Court (Just player) arrow
processInputCourt (Right (Just (Right arrow))) (Court player _) =
  Court player (Just arrow)

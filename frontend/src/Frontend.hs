{-# language
  RecursiveDo,
  TemplateHaskell
#-}

module Frontend where

import qualified Edit
import Edit (PlayerTimelines (PlayerTimelines), Timeline (Timeline), Range (Range), Time (Time))
import qualified View
import View (PlayerMovement (PlayerMovement), Animation (Animation))

import qualified Data.Map.Strict as M
import Common.Route (FrontendRoute)
import Obelisk.Frontend
  ( Frontend (Frontend, _frontend_body, _frontend_head),
  )
import Obelisk.Generated.Static (static)
import Obelisk.Route (R)
import Reflex.Dom.Core hiding (setValue, rangeInput)
import Std

frontend :: Frontend (R FrontendRoute)
frontend =
  Frontend
    {
      _frontend_head = do
        el "title" $ text "movezz"
        elAttr
          "link"
          (
            "href" =: $(static "main.css") <>
            "type" =: "text/css" <>
            "rel" =: "stylesheet"
          )
          blank
        elAttr "link" ("href" =: "https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" <> "rel" =: "stylesheet") blank
        elAttr "link" ("href" =: "https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css" <> "rel" =: "stylesheet") blank,
      _frontend_body = mdo
        ((b, v), _) <- runWithReplace
          (
            (,)
              <$> (
                fmap (domEvent Click) $
                fmap fst $
                elAttr' "button" ("style" =: "float:right") $
                text "animate"
              )
              <*> fmap current Edit.view
          )
          (
            fmap View.render $
            tag (convert <$> v) $
            gate (notEmpty <$> v) $
            b
          )
        blank
    }

convert :: PlayerTimelines -> Animation
convert (PlayerTimelines playerTimelines) =
  Animation
    (zipWith
      (\(Edit.Player p) (s, e) -> (View.Player p, s, e))
      (M.keys playerTimelines)
      [(0, 1), (2, 4)]
    )
    (
      (=<<) -- [] Monad
        (\(Edit.Player p, Timeline t) ->
          fmap
            (\(Range a (Time s) (Time e)) ->
              PlayerMovement
                (View.Player p)
                (convertArrow a)
                (fromInteger s)
                (fromInteger e)
            ) $
          toList $
          t
        ) $
      M.toList $
      playerTimelines
    )

convertArrow :: Edit.Arrow -> View.Arrow
convertArrow (Edit.Arrow t) =
  case t of
    "arrow0" -> View.Arrow t (320, 620) (670, 620)
    "arrow1" -> View.Arrow t (930, 90) (650, 550)
    "arrow2" -> View.Arrow t (60, 110) (890, 80)
    "arrow3" -> View.Arrow t (820, 170) (700, 400)
    "arrow4" -> View.Arrow t (670, 160) (820, 170)
    "arrow5" -> View.Arrow t (370, 480) (360, 430)
    "arrow6" -> View.Arrow t (650, 550) (330, 170)
    "arrow7" -> View.Arrow t (510, 740) (330, 630)
    _ -> error "impossible. the current svg contains no other arrow ids"

notEmpty :: PlayerTimelines -> Bool
notEmpty (PlayerTimelines t) = not (null t)

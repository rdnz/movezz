## Build

1. Follow step 1 and 2 of https://github.com/obsidiansystems/obelisk/#installing-obelisk. There is no need to perform step 3.
2. Execute

```shell
mkdir result
ln -s $(nix-build -A exe --no-out-link)/* result/
cp -r config result
(cd result && ./backend)
```

as described on https://github.com/obsidiansystems/obelisk#locally.

## develop

[install obelisk](https://github.com/obsidiansystems/obelisk#installing-obelisk). the following options are available.

- execute `ob run` for a ghcid window. the web server is updated automatically. `-- $>` code comments are supported.
- execute `ob repl` for a ghci prompt.
- execute `(cd frontend && ob shell 'cabal run tests')` to run the test suite in `frontend/test/Spec.hs`.
- install [`ghcid`](https://github.com/ndmitchell/ghcid#readme) then execute `(cd frontend && ob shell 'ghcid -ac "cabal repl tests"')` for a ghcid window for the test suite in `frontend/test/Spec.hs`.
